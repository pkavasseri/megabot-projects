from megapi import MegaPi
import getch
import sys


MFR = 2     # port for motor front right
MBL = 3     # port for motor back left
MBR = 10    # port for motor back right
MFL = 11    # port for motor front left


class MegaPiController:
    def __init__(self, port='/dev/ttyUSB0', verbose=True):
        self.port = port
        self.verbose = verbose
        if verbose:
            self.printConfiguration()
        self.bot = MegaPi()
        self.bot.start(port=port)
        self.mfr = MFR  # port for motor front right
        self.mbl = MBL  # port for motor back left
        self.mbr = MBR  # port for motor back right
        self.mfl = MFL  # port for motor front left   

    
    def printConfiguration(self):
        print('MegaPiController:')
        print("Communication Port:" + repr(self.port))
        print("Motor ports: MFR: " + repr(MFR) +
              " MBL: " + repr(MBL) + 
              " MBR: " + repr(MBR) + 
              " MFL: " + repr(MFL))


    def setFourMotors(self, vfl=0, vfr=0, vbl=0, vbr=0):
        if self.verbose:
            print("Set Motors: vfl: " + repr(int(round(vfl,0))) + 
                  " vfr: " + repr(int(round(vfr,0))) +
                  " vbl: " + repr(int(round(vbl,0))) +
                  " vbr: " + repr(int(round(vbr,0))))
        self.bot.motorRun(self.mfl,vfl)
        self.bot.motorRun(self.mfr,vfr)
        self.bot.motorRun(self.mbl,vbl)
        self.bot.motorRun(self.mbr,vbr)


    def carStop(self):
        if self.verbose:
            print("CAR STOP:")
        self.setFourMotors()


    def carStraight(self, speed):
        if self.verbose:
            print("CAR STRAIGHT:")
        self.setFourMotors(-speed, speed, -speed, speed)

    def carBack(self, speed):
        if self.verbose:
            print("CAR BACK:")
        self.setFourMotors(speed, -speed, speed, -speed)

    def carRotateLeft(self, speed):
        if self.verbose:
            print("CAR ROTATE LEFT:")
        self.setFourMotors(speed, speed, speed, speed)

    def carRotateRight(self, speed):
        if self.verbose:
            print("CAR ROTATE RIGHT:")
        self.setFourMotors(-speed, -speed, -speed, -speed)


    def carSlideLeft(self, speed):
        if self.verbose:
            print("CAR SLIDE LEFT:")
        self.setFourMotors(speed, speed, -speed, -speed)

    def carSlideRight(self, speed):
        if self.verbose:
            print("CAR SLIDE RIGHT:")
        self.setFourMotors(-speed, -speed, speed, speed)

    
    def carMixed(self, v_straight, v_rotate, v_slide):
        if self.verbose:
            print("CAR MIXED")
        self.setFourMotors(
            v_rotate-v_straight+v_slide,
            v_rotate+v_straight+v_slide,
            v_rotate-v_straight-v_slide,
            v_rotate+v_straight-v_slide
        )
    
    def close(self):
        self.bot.close()
        self.bot.exit()


if __name__ == "__main__":
    import time
    mpi_ctrl = MegaPiController(port='/dev/ttyUSB0', verbose=True)

    print()
    print("enter kyb commands")
    while (True):
        KBD_INPUT =  getch.getch().upper()
        if KBD_INPUT == 'Q':
            # stop all motors and exit
            mpi_ctrl.carStop()
            break;
        if KBD_INPUT == 'I':
            # go straight 
            mpi_ctrl.carStraight(30)
        if KBD_INPUT == 'M':
            # go straight 
            mpi_ctrl.carBack(30)
        if KBD_INPUT == 'J':
            # rotate left 
            mpi_ctrl.carRotateLeft(30)
        if KBD_INPUT == 'L':
            # rotate left 
            mpi_ctrl.carRotateRight(30)
        if KBD_INPUT == 'U':
            # slide
            mpi_ctrl.carSlideLeft(30)
        if KBD_INPUT == 'O':
            # slide
            mpi_ctrl.carSlideRight(30)
        if KBD_INPUT == 'P':
            # stop
            mpi_ctrl.carStop()



    
    # print("If your program cannot be closed properly, check updated instructions in google doc.")
