import socket

# Create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Get local machine name
host = socket.gethostname()

# Reserve a port for your service.
port = 57986

# Connect to the server
s.connect((host, port))

# Receive data from the server
data = s.recv(1024).decode()

# Print the received data
print("Received data from the server: ", data)

# Close the connection
s.close()