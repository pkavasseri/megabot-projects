import socket

# Create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Get local machine name
host = socket.gethostname()

# Reserve a port for your service.
port = 57986

# Bind to the port
s.bind((host, port))

# Wait for a client to connect
s.listen(1)
print("Waiting for a client to connect...")

# Establish a connection with the client
conn, address = s.accept()
print("Connected to client at address: ", address)

# Send data to the client
data = "Hello, client! This is a message from the server."
conn.send(data.encode())

# Close the connection
conn.close()